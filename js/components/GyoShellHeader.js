import { GyoUi } from './GyoUi.js'
import { GyoStyle } from './GyoStyle.js'

export class GyoShellHeader extends GyoUi {
  constructor(style) {
    super(style)
  }

  getElement() {
    const style = this._style

    return class GyoShellHeaderElement extends HTMLElement {
      constructor() {
        super()
        const shadowRoot = this.attachShadow({ mode: 'open' })
        shadowRoot.innerHTML = `
          <style>
            :host {
              ${style.getHostDefaultStyle()}
              flex-shrink: 0;
            }

            #gyo-shell-header {
              display: flex;
              justify-content: center;
              align-items: baseline;
              flex-wrap: wrap;
              padding: ${style.unit / 2}px ${style.unit / 2}px ${style.unit * 1.5}px;
            }

            #gyo-shell-header__title {
              flex-shrink: 0;
              padding: ${style.unit / 2}px;
              font-family: 'Katibeh', cursive;
              font-size: ${style.fontSizeShellTitle};
              font-weight: ${style.fontWeightB};
              color: ${style.color5};
            }

            #gyo-shell-header__profile {
              flex-shrink: 0;
              padding: ${style.unit / 2}px;
              font-family: 'Katibeh', cursive;
              font-size: ${style.fontSizeL};
              color: ${style.getDimmedColor('color5')};
              white-space: nowrap;
            }
          </style>
          <div id="gyo-shell-header">
            <div id="gyo-shell-header__title">
              Gyo
            </div>

            <div id="gyo-shell-header__profile">
              Front End Engineer
            </div>
          </div>
        `
      }
    }
  }
}

window.customElements.define('gyo-shell-header', new GyoShellHeader(new GyoStyle()).getElement())
