import { GyoUi } from './GyoUi.js'
import { GyoStyle } from './GyoStyle.js'

export class GyoFormItem extends GyoUi {
  constructor(style) {
    super(style)
  }

  getElement() {
    const style = this._style

    return class GyoFormItemElement extends HTMLElement {
      constructor() {
        super()
        const shadowRoot = this.attachShadow({ mode: 'open' })
        shadowRoot.innerHTML = `
          <style>
            :host {
              ${style.getHostDefaultStyle()}
            }
          </style>
  
          <div>
            <slot name="label"></slot>

            <slot></slot>
          </div>
        `
      }
    }
  }
}

window.customElements.define('gyo-form-item', new GyoFormItem(new GyoStyle()).getElement())
