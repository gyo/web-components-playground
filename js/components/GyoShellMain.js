import { GyoUi } from './GyoUi.js'
import { GyoStyle } from './GyoStyle.js'

export class GyoShellMain extends GyoUi {
  constructor(style) {
    super(style)
  }

  getElement() {
    const style = this._style

    return class GyoShellMainElement extends HTMLElement {
      constructor() {
        super()
        const shadowRoot = this.attachShadow({ mode: 'open' })
        shadowRoot.innerHTML = `
          <style>
            :host {
              ${style.getHostDefaultStyle()}
              flex-grow: 1;
            }
          </style>
          <main><slot></slot></main>
        `
      }
    }
  }
}

window.customElements.define('gyo-shell-main', new GyoShellMain(new GyoStyle()).getElement())
