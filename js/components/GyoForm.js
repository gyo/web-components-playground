import { GyoUi } from './GyoUi.js'
import { GyoStyle } from './GyoStyle.js'

export class GyoForm extends GyoUi {
  constructor(style) {
    super(style)
  }

  getElement() {
    const style = this._style

    return class GyoFormElement extends HTMLElement {
      constructor() {
        super()
        const methodAttributeText = this.hasAttribute('method') ? `href="${this.getAttribute('method')}"` : ''
        const actionAttributeText = this.hasAttribute('action') ? `target="${this.getAttribute('action')}"` : ''
        const shadowRoot = this.attachShadow({ mode: 'open' })
        shadowRoot.innerHTML = `
          <style>
            :host {
              ${style.getHostDefaultStyle()}
            }

            form {
              margin: 0;
            }
          </style>
  
          <form ${methodAttributeText} ${actionAttributeText}>
            <input type="text" name="e-mail3">
            <button>SUBMIT</button>
          </form>
        `
      }
    }
  }
}

window.customElements.define('gyo-form', new GyoForm(new GyoStyle()).getElement())
