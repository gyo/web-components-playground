export default class ZIndexComponent extends HTMLElement {
  constructor() {
    // console.log('ZIndexComponent: constructor')
    super()
    const shadowRoot = this.attachShadow({ mode: 'open' })
    shadowRoot.innerHTML = `
      <style>
        :host {
        }

        div {
          position: fixed;
          top: 10vh;
          left: 10vw;
          width: 80vw;
          height: 80vh;
          z-index: 1;
          background: rgba(0, 0, 0, .1)
        }
      </style>
      <div>Float</div>
    `
  }
}
