import { GyoUi } from './GyoUi.js'
import { GyoStyle } from './GyoStyle.js'
import './GyoLink.js'

export class GyoShellFooter extends GyoUi {
  constructor(style) {
    super(style)
  }

  getElement() {
    const style = this._style

    return class GyoShellFooterElement extends HTMLElement {
      constructor() {
        super()
        const shadowRoot = this.attachShadow({ mode: 'open' })
        shadowRoot.innerHTML = `
          <style>
            :host {
              ${style.getHostDefaultStyle()}
              flex-shrink: 0;
            }

            div {
              display: flex;
              flex-direction: column;
              justify-content: center;
              align-items: center;
              padding: ${style.unit}px;
              font-size: ${style.fontSizeS};
              color: ${style.color1};
            }
          </style>
          <div>
            <slot></slot>
            <gyo-link href="#">Thank you for reading!</gyo-link>
          </div>
        `
      }
    }
  }
}

window.customElements.define('gyo-shell-footer', new GyoShellFooter(new GyoStyle()).getElement())
