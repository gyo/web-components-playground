module.exports = {
  root: true,
  extends: ['eslint:recommended', 'prettier'],
  env: {
    browser: true,
    es6: true,
    commonjs: true
  },
  parserOptions: {
    ecmaVersion: 2018,
    ecmaFeatures: {
      experimentalObjectRestSpread: true
    },
    sourceType: 'module'
  },
  plugins: ['prettier'],
  rules: {
    'no-console': 0,
    'prettier/prettier': 'error'
  }
}
