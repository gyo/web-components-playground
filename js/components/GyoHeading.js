import { GyoUi } from './GyoUi.js'
import { GyoStyle } from './GyoStyle.js'

export class GyoHeading extends GyoUi {
  constructor(style) {
    super(style)
  }

  getElement() {
    const style = this._style

    return class GyoHeadingElement extends HTMLElement {
      constructor() {
        super()
        const tag = this.hasAttribute('tag') ? this.getAttribute('tag') : 'h1'
        const shadowRoot = this.attachShadow({ mode: 'open' })
        shadowRoot.innerHTML = `
          <style>
            :host {
              ${style.getHostDefaultStyle()}
            }
    
            h1, h2, h3, h4, h5, h6 {
              margin: 0;
              color: ${style.color5};
            }
          </style>
          <${tag}><slot></slot></${tag}>
        `
      }
    }
  }
}

window.customElements.define('gyo-heading', new GyoHeading(new GyoStyle()).getElement())
