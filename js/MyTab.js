export default class MyTab extends HTMLElement {
  constructor() {
    // console.log('MyTab: constructor')
    super()

    this._data = {
      _tabs: null,
      _panels: null,
      _selectedIndex: null,
      _boundOnTabClick: null
    }

    const shadowRoot = this.attachShadow({ mode: 'open' })
    shadowRoot.innerHTML = `
      <link rel="stylesheet" href="./css/MyTab.css">
      
      <div>
        <slot id="tab" name="tab"></slot>
      </div>
      
      <section>
        <slot id="panel"></slot>
      </section>
    `
  }

  connectedCallback() {
    const tab = this.shadowRoot.querySelector('#tab')
    const panel = this.shadowRoot.querySelector('#panel')

    this._data._tabs = tab.assignedNodes({ flatten: true }).filter(element => {
      return element.nodeType === Node.ELEMENT_NODE
    })
    this._data._panels = panel.assignedNodes({ flatten: true }).filter(element => {
      return element.nodeType === Node.ELEMENT_NODE
    })

    this._data._boundOnTabClick = this._onTabClick.bind(this)
    tab.addEventListener('click', this._data._boundOnTabClick)

    // console.log([this])
    // console.log(this._data._tabs)
    // console.log(this._data._panels)
  }

  _onTabClick(e) {
    this._data._selectedIndex = this._data._tabs.indexOf(e.target)
    this._selectTab()

    // console.log([this])
    // console.log(this._data._selectedIndex)
  }

  _selectTab() {
    if (this._data._selectedIndex === null) {
      return
    }
    this._data._panels.forEach((panel, index) => {
      if (index === this._data._selectedIndex) {
        panel.removeAttribute('hidden')
      } else {
        panel.setAttribute('hidden', '')
      }
    })
  }
}
