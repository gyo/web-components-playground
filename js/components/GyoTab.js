import { GyoUi } from './GyoUi.js'
import { GyoStyle } from './GyoStyle.js'

export class GyoTab extends GyoUi {
  constructor(style) {
    super(style)
  }

  getElement() {
    const style = this._style

    return class GyoTabElement extends HTMLElement {
      constructor() {
        super()

        this._data = {
          _tabs: [],
          _panels: [],
          _selectedIndex: 0,
          _boundOnTabClick: () => {}
        }

        const shadowRoot = this.attachShadow({ mode: 'open' })
        shadowRoot.innerHTML = `
          <style>
            :host {
              ${style.getHostDefaultStyle()}
            }

            #tabs-wrapper {
              display: flex;
              justify-content: center;
            }

            #tabs {
              display: flex;
              overflow: auto;
            }

            #tabSlot::slotted([selected]) {
              background: ${style.color1};
            }

            #panelSlot::slotted([hidden]) {
              display: none;
            }
          </style>

          <div id="tabs-wrapper">
            <div id="tabs">
              <slot id="tabSlot" name="tab"></slot>
            </div>
          </div>

          <slot id="panelSlot"></slot>
        `
      }

      connectedCallback() {
        const tabSlot = this.shadowRoot.querySelector('#tabSlot')
        const panelSlot = this.shadowRoot.querySelector('#panelSlot')

        this._data._tabs = tabSlot.assignedNodes({ flatten: true }).filter(element => {
          return element.nodeType === Node.ELEMENT_NODE
        })
        this._data._panels = panelSlot.assignedNodes({ flatten: true }).filter(element => {
          return element.nodeType === Node.ELEMENT_NODE
        })

        this._data._boundOnTabClick = this._onTabClick.bind(this)
        tabSlot.addEventListener('click', this._data._boundOnTabClick)
        this._selectTab()
      }

      _onTabClick(e) {
        this._data._selectedIndex = this._data._tabs.indexOf(e.target)
        this._selectTab()
      }

      _selectTab() {
        this._data._tabs.forEach((tab, index) => {
          if (index === this._data._selectedIndex) {
            tab.setAttribute('selected', '')
          } else {
            tab.removeAttribute('selected')
          }
        })
        this._data._panels.forEach((panel, index) => {
          if (index === this._data._selectedIndex) {
            panel.removeAttribute('hidden')
          } else {
            panel.setAttribute('hidden', '')
          }
        })
      }
    }
  }
}

window.customElements.define('gyo-tab', new GyoTab(new GyoStyle()).getElement())
