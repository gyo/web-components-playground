import { GyoUi } from './GyoUi.js'
import { GyoStyle } from './GyoStyle.js'

export class GyoTextField extends GyoUi {
  constructor(style) {
    super(style)
  }

  getElement() {
    const style = this._style

    return class GyoTextFieldElement extends HTMLElement {
      constructor() {
        super()
        const type = this.hasAttribute('type') ? this.getAttribute('type') : 'text'
        const name = this.hasAttribute('name') ? this.getAttribute('name') : ''
        const shadowRoot = this.attachShadow({ mode: 'open' })
        shadowRoot.innerHTML = `
          <style>
            :host {
              ${style.getHostDefaultStyle()}
            }
    
            input {
              display: block;
              width: 100%;
              padding: 0.5em 1em;
              border: 1px solid ${style.color1};
              border-radius: ${style.unit / 4}px;
              color: ${style.color5};
              font-family: inherit;
              font-weight: ${style.fontWeightN};
              font-size: inherit;
            }
          </style>
          <input type="${type}" name="${name}">
        `
      }
    }
  }
}

window.customElements.define('gyo-text-field', new GyoTextField(new GyoStyle()).getElement())
