export class GyoStyle {
  constructor() {
    this.unit = 16
    this.fontFamily = 'inherit'
    this.fontSizeShellTitle = `${this.unit * 1.5 * 1.5 * 1.5}px`
    this.fontSizeLL = `${this.unit * 1.5 * 1.5}px`
    this.fontSizeL = `${this.unit * 1.5}px`
    this.fontSizeM = `${this.unit}px`
    this.fontSizeS = `${this.unit / 1.5}px`
    this.fontWeightB = 'bold'
    this.fontWeightN = 'normal'
    this.lineHeight = '1.5'
    this.color1 = 'hsla(240, 0%, 80%, 1)'
    this.color2 = 'hsla(240, 0%, 64%, 1)'
    this.color3 = 'hsla(240, 0%, 51%, 1)'
    this.color4 = 'hsla(240, 0%, 41%, 1)'
    this.color5 = 'hsla(240, 0%, 33%, 1)'
    this.colorWhite = 'hsla(0, 0%, 100%, 1)'
    this.colorLink = this.color3
  }

  getHostDefaultStyle() {
    return `
      all: initial;
      display: block;
      font-family: ${this.fontFamily};
      font-size: ${this.fontSizeM};
      font-weight: ${this.fontWeightN};
      line-height: ${this.lineHeight};
      font-feature-settings: 'palt';
      box-sizing: border-box;
    `
  }

  getDimmedColor(colorName) {
    const tmpRegExpResult = this[colorName].match(/hsla\((.*,\s*?)([^,]*?)\)/)

    const hsl = tmpRegExpResult[1]
    const alpha = Number.parseInt(tmpRegExpResult[2])

    const dimmedAlpha = alpha * 0.8

    return `hsla(${hsl} ${dimmedAlpha})`
  }
}
