import './components/GyoHeading.js'
import './components/GyoLabel.js'
import './components/GyoParagraph.js'
import './components/GyoSection.js'
import './components/GyoShell.js'
import './components/GyoShellMain.js'

const signInButton = document.querySelector('#sign-in-button')
const signOutButton = document.querySelector('#sign-out-button')

if (navigator.credentials && navigator.credentials.preventSilentAccess) {
  // get old cred
  navigator.credentials
    .get({
      password: true
    })
    .then(cred => {
      console.log('Old cred is', cred)
    })

  // create new cred
  const cred = new PasswordCredential({
    id: 'FAKEID',
    password: `${new Date().getTime()}`
  })

  console.log(`New cred is`, cred)

  // update and get new cred
  navigator.credentials.store(cred).then(() => {
    console.log('Store new cred, then...')
  })

  signInButton.addEventListener('click', e => {
    e.preventDefault()
    const form = document.querySelector('#form')
    const cred = new PasswordCredential({
      id: form.userId.value,
      password: form.password.value
    })

    console.log(`New cred is`, cred)

    navigator.credentials.store(cred).then(() => {
      console.log('Store new cred, then...')
    })
  })

  signOutButton.addEventListener('click', e => {
    e.preventDefault()
    navigator.credentials.preventSilentAccess()
  })
}
