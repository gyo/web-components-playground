import MyComponent from './MyComponent.js'
import ChildComponent from './ChildComponent.js'
import TemplateComponent from './TemplateComponent.js'
import TemplateSlot from './TemplateSlot.js'
import MyTab from './MyTab.js'
import ZIndexComponent from './ZIndexComponent.js'
import MyElement from './MyElement.js'

const undefinedComponents = document.querySelectorAll(':not(:defined)')
const promises = Array.from(undefinedComponents).map(undefinedComponent => {
  return customElements.whenDefined(undefinedComponent.localName)
})
Promise.all(promises).then(() => {
  // console.log('All customElements were defined')
})

// console.log(document.querySelectorAll(':not(:defined)'), 'are undefined')

window.customElements.define('my-component', MyComponent)
window.customElements.define('child-component', ChildComponent)
window.customElements.define('template-component', TemplateComponent)
window.customElements.define('template-slot', TemplateSlot)
window.customElements.define('my-tab', MyTab)
window.customElements.define('z-index-component', ZIndexComponent)
window.customElements.define('my-element', MyElement)

// console.log(document.querySelectorAll(':not(:defined)'), 'are undefined')

const myComponent = document.querySelector('my-component')
const childComponent = document.querySelector('child-component')
const myTab = document.querySelector('my-tab')

document.querySelector('.js-toggle-disabled').addEventListener('click', () => {
  myComponent.disabled = !myComponent.disabled
})

document.querySelector('.js-toggle-child-disabled').addEventListener('click', () => {
  childComponent.disabled = !childComponent.disabled
})

document.querySelector('.js-add-tag').addEventListener('click', () => {
  const now = Date.now()
  const tab = document.createElement('div')
  tab.setAttribute('slot', 'tab')
  tab.textContent = `TAB${now}`
  const content = document.createElement('div')
  content.textContent = `Content${now}`
  myTab.appendChild(tab)
  myTab.appendChild(content)
})

// console.log([document.querySelector('my-tab div:last-child').assignedSlot])

const shadowElement = document.createElement('div')
const shadowRoot = shadowElement.attachShadow({ mode: 'open' })
shadowRoot.innerHTML = `
  <p>Hello World!!</p>
  <p>I am Shadow DOM</p>
`

document.body.appendChild(shadowElement)
