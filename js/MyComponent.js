export default class MyComponent extends HTMLElement {
  constructor() {
    // console.log('MyComponent: constructor')
    super()
    this.setAttribute('tabindex', '0')
    const shadowRoot = this.attachShadow({ mode: 'open' })
    shadowRoot.innerHTML = `
      <style>
        :host {
          all: initial;
          display: block;
          background: aliceblue;
        }

        :host([disabled]) {
          opacity: .3;
          pointer-events: none;
        }

        div {
          color: slateblue;
        }
      </style>
      <div>Shadow DOM</div>
      <slot></slot>
    `
  }

  static get observedAttributes() {
    return ['disabled']
  }

  get disabled() {
    return this.hasAttribute('disabled')
  }

  set disabled(val) {
    if (val) {
      this.setAttribute('disabled', '')
    } else {
      this.removeAttribute('disabled')
    }
  }

  connectedCallback() {
    // console.log('MyComponent: connectedCallback')
  }

  disconnectedCallback() {
    // console.log('MyComponent: disconnectedCallback')
  }

  attributeChangedCallback(name, oldValue, newValue) {
    // console.log('MyComponent: attributeChangedCallback')

    if (name === 'disabled' && oldValue === null && newValue === '') {
      this.setAttribute('tabindex', '-1')
      return
    }

    if (name === 'disabled' && oldValue === '' && newValue === null) {
      this.setAttribute('tabindex', '0')
      return
    }
  }
}
