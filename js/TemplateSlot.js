export default class TemplateSlot extends HTMLElement {
  constructor() {
    // console.log('TemplateSlot: constructor')
    super()
    const shadowRoot = this.attachShadow({ mode: 'open' })
    const template = document.querySelector('#slot-template')
    const instance = template.content.cloneNode(true)
    shadowRoot.appendChild(instance)
  }
}
