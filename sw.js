var CACHE_NAME = 'cache-v1'
var urlsToCache = [
  './index.html',
  './css/normalize.css',
  './css/index.css',
  './js/index.js',
  './js/components/GyoButton.js',
  './js/components/GyoGroup.js',
  './js/components/GyoHeading.js',
  './js/components/GyoLink.js',
  './js/components/GyoList.js',
  './js/components/GyoListItem.js',
  './js/components/GyoParagraph.js',
  './js/components/GyoSection.js',
  './js/components/GyoShell.js',
  './js/components/GyoShellFooter.js',
  './js/components/GyoShellHeader.js',
  './js/components/GyoShellMain.js',
  './js/components/GyoSkill.js',
  './js/components/GyoStyle.js',
  './js/components/GyoTab.js',
  './js/components/GyoUi.js'
]

self.addEventListener('install', event => {
  console.log('[SW] install', event)

  event.waitUntil(
    caches.open(CACHE_NAME).then(function(cache) {
      console.log('[SW] caches.open().then()', cache)
      return cache.addAll(urlsToCache)
    })
  )
})

self.addEventListener('fetch', event => {
  console.log('[SW] fetch', event.request.url)

  event.respondWith(
    caches.match(event.request).then(response => {
      console.log('[SW] caches.match().then()', response)

      return response ? response : fetch(event.request)
    })
  )
})
