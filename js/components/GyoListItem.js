import { GyoUi } from './GyoUi.js'
import { GyoStyle } from './GyoStyle.js'

export class GyoListItem extends GyoUi {
  constructor(style) {
    super(style)
  }

  getElement() {
    const style = this._style

    return class GyoListItemElement extends HTMLElement {
      constructor() {
        super()
        const shadowRoot = this.attachShadow({ mode: 'open' })
        shadowRoot.innerHTML = `
          <style>
            :host {
              ${style.getHostDefaultStyle()}
            }
    
            li {
              color: ${style.color5};
            }
          </style>
          <li><slot></slot></li>
        `
      }
    }
  }
}

window.customElements.define('gyo-list-item', new GyoListItem(new GyoStyle()).getElement())
