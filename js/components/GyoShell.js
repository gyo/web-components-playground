import { GyoUi } from './GyoUi.js'
import { GyoStyle } from './GyoStyle.js'

export class GyoShell extends GyoUi {
  constructor(style) {
    super(style)
  }

  getElement() {
    const style = this._style

    return class GyoShellElement extends HTMLElement {
      constructor() {
        super()
        const shadowRoot = this.attachShadow({ mode: 'open' })
        shadowRoot.innerHTML = `
          <style>
            :host {
              ${style.getHostDefaultStyle()}
              min-height: 100vh;
              display: flex;
              flex-direction: column;
            }
          </style>
          <slot></slot>
        `
      }
    }
  }
}

window.customElements.define('gyo-shell', new GyoShell(new GyoStyle()).getElement())
