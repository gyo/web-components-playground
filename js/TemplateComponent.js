export default class MyComponent extends HTMLElement {
  constructor() {
    // console.log('TemplateComponent: constructor')
    super()
    const shadowRoot = this.attachShadow({ mode: 'open' })
    const template = document.querySelector('#my-template')
    const instance = template.content.cloneNode(true)
    shadowRoot.appendChild(instance)
  }
}
