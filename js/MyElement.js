class MyElement extends HTMLElement {
  constructor() {
    super()

    this.innerHTML = `
      <p>Hello World!!</p>
      <p>I am MyElement</p>
    `
  }
}

export default MyElement
