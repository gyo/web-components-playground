import './components/GyoStyle.js'
import './components/GyoButton.js'
import './components/GyoGroup.js'
import './components/GyoHeading.js'
import './components/GyoLink.js'
import './components/GyoList.js'
import './components/GyoListItem.js'
import './components/GyoParagraph.js'
import './components/GyoSection.js'
import './components/GyoShell.js'
import './components/GyoShellFooter.js'
import './components/GyoShellHeader.js'
import './components/GyoShellMain.js'
import './components/GyoSkill.js'
import './components/GyoTab.js'

const buttonUpdateServiceWorker = document.querySelector('#button-update-service-worker')

if ('serviceWorker' in navigator) {
  navigator.serviceWorker
    .register('./sw.js')
    .then(registration => {
      // 登録成功
      console.log('ServiceWorker registration successful with scope: ', registration.scope)

      buttonUpdateServiceWorker.addEventListener('click', () => {
        registration.update()
      })
    })
    .catch(err => {
      // 登録失敗
      console.log('ServiceWorker registration failed: ', err)
    })
}
