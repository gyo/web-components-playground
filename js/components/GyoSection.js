import { GyoUi } from './GyoUi.js'
import { GyoStyle } from './GyoStyle.js'

export class GyoSection extends GyoUi {
  constructor(style) {
    super(style)
  }

  getElement() {
    const style = this._style

    return class GyoSectionElement extends HTMLElement {
      constructor() {
        super()
        const shadowRoot = this.attachShadow({ mode: 'open' })
        shadowRoot.innerHTML = `
          <style>
            :host {
              ${style.getHostDefaultStyle()}
              margin: 0 auto;
              max-width: ${style.unit * 24}px;
              padding: ${style.unit / 2}px;
            }
            
            div {
              padding: ${style.unit / 2}px;
            }
          </style>
          <section>
            <div>
              <slot name="heading"></slot>
            </div>

            <div>
              <slot name="lead"></slot>
            </div>

            <div>
              <slot></slot>
            </div>
          </section>
        `
      }

      connectedCallback() {
        const headingSlot = this.shadowRoot.querySelector('slot[name="heading"]')
        if (
          headingSlot.assignedNodes({ flatten: true }).filter(element => {
            return element.nodeType === Node.ELEMENT_NODE
          }).length === 0
        ) {
          headingSlot.parentNode.style.display = 'none'
        }

        const leadSlot = this.shadowRoot.querySelector('slot[name="lead"]')
        if (
          leadSlot.assignedNodes({ flatten: true }).filter(element => {
            return element.nodeType === Node.ELEMENT_NODE
          }).length === 0
        ) {
          leadSlot.parentNode.style.display = 'none'
        }
      }
    }
  }
}

window.customElements.define('gyo-section', new GyoSection(new GyoStyle()).getElement())
