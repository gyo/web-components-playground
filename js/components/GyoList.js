import { GyoUi } from './GyoUi.js'
import { GyoStyle } from './GyoStyle.js'

export class GyoList extends GyoUi {
  constructor(style) {
    super(style)
  }

  getElement() {
    const style = this._style

    return class GyoListElement extends HTMLElement {
      constructor() {
        super()
        const shadowRoot = this.attachShadow({ mode: 'open' })
        shadowRoot.innerHTML = `
          <style>
            :host {
              ${style.getHostDefaultStyle()}
            }

            ul {
              margin: 0;
            }
          </style>
          <ul>
            <slot></slot>
          </ul>
        `
      }
    }
  }
}

window.customElements.define('gyo-list', new GyoList(new GyoStyle()).getElement())
