import { GyoUi } from './GyoUi.js'
import { GyoStyle } from './GyoStyle.js'

export class GyoParagraph extends GyoUi {
  constructor(style) {
    super(style)
  }

  getElement() {
    const style = this._style

    return class GyoParagraphElement extends HTMLElement {
      constructor() {
        super()
        const shadowRoot = this.attachShadow({ mode: 'open' })
        shadowRoot.innerHTML = `
          <style>
            :host {
              ${style.getHostDefaultStyle()}
            }
    
            p {
              margin: 0;
              color: ${style.color5};
            }
          </style>
          <p><slot></slot></p>
        `
      }
    }
  }
}

window.customElements.define('gyo-paragraph', new GyoParagraph(new GyoStyle()).getElement())
