import { GyoUi } from './GyoUi.js'
import { GyoStyle } from './GyoStyle.js'

export class GyoLink extends GyoUi {
  constructor(style) {
    super(style)
  }

  getElement() {
    const style = this._style

    return class GyoLinkElement extends HTMLElement {
      constructor() {
        super()
        const hrefAttributeText = this.hasAttribute('href') ? `href="${this.getAttribute('href')}"` : ''
        const targetAttributeText = this.hasAttribute('target') ? `target="${this.getAttribute('target')}"` : ''
        const shadowRoot = this.attachShadow({ mode: 'open' })
        shadowRoot.innerHTML = `
          <style>
            :host {
              ${style.getHostDefaultStyle()}
              display: inline;
            }

            a {
              color: ${style.colorLink};
            }
          </style>
          <a ${hrefAttributeText} ${targetAttributeText}><slot></slot></a>
        `
      }
    }
  }
}

window.customElements.define('gyo-link', new GyoLink(new GyoStyle()).getElement())
