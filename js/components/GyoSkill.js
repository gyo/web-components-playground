import { GyoUi } from './GyoUi.js'
import { GyoStyle } from './GyoStyle.js'

export class GyoSkill extends GyoUi {
  constructor(style) {
    super(style)
  }

  getElement() {
    const style = this._style

    return class GyoSkillElement extends HTMLElement {
      constructor() {
        super()
        const level = this.hasAttribute('level') ? Number.parseInt(this.getAttribute('level')) : 0
        const starsHtml = this._getStarsHtml(level)
        const emptyStarsHtml = this._getEmptyStarsHtml(level)
        const shadowRoot = this.attachShadow({ mode: 'open' })
        shadowRoot.innerHTML = `
          <style>
            :host {
              ${style.getHostDefaultStyle()}
            }

            section {
              display: flex;
            }

            #stars {
              display: flex;
            }
          </style>
          <section>
            <div id="text">
              <slot></slot>
            </div>
            <div id="stars">
              ${starsHtml}
              ${emptyStarsHtml}
            </div>
          </section>
        `
      }

      _getStarsHtml(level) {
        return new Array(level)
          .fill('')
          .map(() => {
            return `
              <div class="star">
                ${this._getStarSvg()}
              </div>
            `
          })
          .join('')
      }

      _getEmptyStarsHtml(level) {
        return new Array(5 - level)
          .fill('')
          .map(() => {
            return `
              <div class="star">
                ${this._getEmptyStarSvg()}
              </div>
            `
          })
          .join('')
      }

      _getStarSvg() {
        return `
          <svg width="24" height="22" viewBox="0 0 24 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <g transform="translate(-475 -34)">
              <use xlink:href="#path0_fill" transform="translate(475.261 34)" fill="${style.color5}"/>
            </g>
            <defs>
              <path id="path0_fill" d="M 12 0L 14.6942 8.2918L 23.4127 8.2918L 16.3593 13.4164L 19.0534 21.7082L 12 16.5836L 4.94658 21.7082L 7.64074 13.4164L 0.587322 8.2918L 9.30583 8.2918L 12 0Z"/>
            </defs>
          </svg>
        `
      }

      _getEmptyStarSvg() {
        return `
          <svg width="24" height="22" viewBox="0 0 24 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <g transform="translate(-475 -34)">
              <use xlink:href="#path0_fill" transform="translate(475.261 34)" fill="${style.color1}"/>
            </g>
            <defs>
              <path id="path0_fill" d="M 12 0L 14.6942 8.2918L 23.4127 8.2918L 16.3593 13.4164L 19.0534 21.7082L 12 16.5836L 4.94658 21.7082L 7.64074 13.4164L 0.587322 8.2918L 9.30583 8.2918L 12 0Z"/>
            </defs>
          </svg>
        `
      }
    }
  }
}

window.customElements.define('gyo-skill', new GyoSkill(new GyoStyle()).getElement())
