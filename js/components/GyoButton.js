import { GyoUi } from './GyoUi.js'
import { GyoStyle } from './GyoStyle.js'

export class GyoButton extends GyoUi {
  constructor(style) {
    super(style)
  }

  getElement() {
    const style = this._style

    return class GyoButtonElement extends HTMLElement {
      constructor() {
        super()
        const tag = this.hasAttribute('tag') ? this.getAttribute('tag') : 'div'
        const border = this.dataset.border === 'true' ? `border: 1px solid ${style.color1};` : 'border: none;'
        const shadowRoot = this.attachShadow({ mode: 'open' })
        shadowRoot.innerHTML = `
          <style>
            :host {
              ${style.getHostDefaultStyle()}
              cursor: pointer;
            }

            #button {
              display: inline-block;
              ${border}
              border-radius: ${style.unit / 4}px;
              padding: 0.5em 1em;
              background: transparent;
              color: ${style.colorLink};
              font-family: inherit;
              font-weight: ${style.fontWeightB};
              white-space: nowrap;
            }
          </style>

          <${tag} id="button">
            <slot></slot>
          </${tag}>
        `
      }
    }
  }
}

window.customElements.define('gyo-button', new GyoButton(new GyoStyle()).getElement())
